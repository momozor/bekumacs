;;  This file is part of bekumacs
;;  Copyright (C) 2019  Momozor

;;  This program is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU Affero General Public License as published
;;  by the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU Affero General Public License for more details.

;;  You should have received a copy of the GNU Affero General Public License
;;  along with this program.  If not, see <https://www.gnu.org/licenses/>.





;;; MELPA

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    ;;(add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))
    ))
(package-initialize)





;;; ELISP LANGUAGE EXTENSIONS

;; enable lexical binding
(setq lexical-binding t)

;;; ELISP config vars

;; please update global vars here
(setq slime-helper-path "~/.roswell/helper.el")
(setq local-projects-path "~/.roswell/local-projects")

;; install required packages
(defun pkgs-installer ()
  (let ((i (y-or-n-p "Do you want to refresh packages from MELPA? ")))
    (when i
      (package-refresh-contents))
    (let ((pkgs (list 'smex
                      'slime
                      'neotree
                      'rust-mode
                      'company
                      'geiser
                      'racer)))
      (mapcar #'package-install pkgs)))
  (let ((u (y-or-n-p "Do you want to append (set-installed-packaged-config-now)? ")))
    (when u
      (write-region "(set-installed-packages-config)"
                    nil
                    "~/.emacs"
                    'append)))
  (message "All set! :-)"))

(defun load-slime-stuff ()
  ;; load SLIME helper

  (when (file-directory-p slime-helper-path)
    (load (expand-file-name slime-helper-path)))

  ;; set default directory to Quicklisp local-projects
  (when (file-directory-p local-projects-path)
    (setq default-directory local-projects-path)))

;; set installed packages config
(defun set-installed-packages-config ()
  ;; auto-complete for programming languages
  ;;(ac-config-default)
  (add-hook 'after-init-hook 'global-company-mode)

  ;; makes M-x command completion easier and beautiful
  (ido-mode)

  (smex-initialize)
  ;; bind M-x with smex
  (global-set-key (kbd "M-x") 'smex)
  (global-set-key (kbd "M-X") 'smex-major-mode-commands)

  ;; comment this if you want to disable load slime path
  (load-slime-stuff))

;; if you don't need to change anything above, run below func
;; to do fresh setup
;; (pkgs-installer)


;; racer
(defun set-rust-racer ()
  (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (add-hook 'racer-mode-hook #'company-mode)

  (require 'rust-mode)
  (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
  (setq company-tooltip-align-annotations t))

;;; EMACS CONFIGURATIONS

;; comment the line below if you are going to do fresh setup
;; which can be done with (emacs-setup-all-now)
(set-installed-packages-config)

;; disable gnu emacs buffer on startup
(setq inhibit-startup-message t)

;; don't create backups
(setq make-backup-files nil)
(setq auto-save-default nil)
(setq create-lockfiles nil)

;; set yes or no prompt with answer y/n
(defalias 'yes-or-no-p 'y-or-n-p)

;; pair parentheses and other characters combination.
                                        ; also highlight parentheses
(electric-pair-mode)
(show-paren-mode t)

;; Use space instead of tab.
;; PLEASE USE SPACE OVER TAB :P
(setq-default indent-tabs-mode nil)

;; highlight characters beyond than 80th column
;; for easily readability of the text
(setq-default whitespace-line-column
              80
              whitespace-style
              '(face lines-tail))
(global-whitespace-mode 1)

;; make line number mode (horizontal)
;; and column number mode (vertical)
;; default for every file
(global-linum-mode t)
(column-number-mode t)

;;; KEY SHORTCUTS

;; window navigation
(global-set-key (kbd "C-x u") 'windmove-up)
(global-set-key (kbd "C-x d") 'windmove-down)
(global-set-key (kbd "C-x p") 'windmove-left)
(global-set-key (kbd "C-x n") 'windmove-right)

;; shrink/expand window
(global-set-key (kbd "S-C-p") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-n") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-u") 'enlarge-window)
(global-set-key (kbd "S-C-d") 'shrink-window)

;; kill buffer & window
(global-set-key (kbd "S-C-k") 'kill-buffer-and-window)

;;; SELF DEFINED FUNCTIONS (or helpers)
;;; INVOKE WITH M-:
;;; EXAMPLE
;;;   (workspace-set "~/workspace/rust/existing-project-dir")

;; set primary workspace with tree view and default dir
;; to path
;; currently won't delete existing bottom terminal
(defun workspace-set (path)
  (setq default-dir path)
  (cd path)
  (neotree-dir path)
  (windmove-right)
  (split-window-below)
  (windmove-down)
  (ansi-term "/bin/bash")
  (windmove-up))

(defun workspace-delete ()
  "delete existing workspace that was invoked with (workspace-set (path))"
  (neotree-hide)
  (kill-buffer "*ansi-term*")
  (kill-buffer-and-window))


;;(set-rust-racer)
